# download OpnAPI at https://www.futunn.com/download/OpenAPI
# pip install futu-api

# e.g. doc at 
# https://openapi.futunn.com/futu-api-doc/quote/request-history-kline.html

"""

开户限制
首先，您需要先在富途牛牛 APP 或 moomoo APP 上，完成交易业务账户的开通，才能成功登录 OpenAPI。 https://openapi.futunn.com/futu-api-doc/intro/authority.html#8164

#合规确认
牛牛用户moomoo 用户
首次登陆成功后，您需要完成问卷评估与协议确认，才能继续使用 OpenAPI。牛牛用户请 点击这里
会根据您账户的资产和交易的情况，下发历史 K 线额度。因此，30 天内您只能获取有限只股票的历史 K 线数据。具体规则参见 订阅额度 & 历史 K 线额度。您当日消耗的历史 K 线额度，会在 30 天后自动释放。 https://www.futunn.com/about/api-disclaimer?lang=zh-cn

订阅额度和历史 K 线额度限制如下：

用户类型	            订阅额度      历史 K 线额度
开户用户	            100	          100
总资产达 1 万 HKD           300	          300
以下三条满足任意一条即可：  
1. 总资产达 50 万 HKD； 2. 交易笔数 > 200； 3. 交易额 > 200 万 HKD
                            1000	  1000
以下三条满足任意一条即可：  
1. 总资产达 500 万 HKD； 2. 交易笔数 > 2000； 3. 交易额 > 2000 万 HKD
                            2000	  2000

历史 K 线额度，适用于 获取历史 K 线 接口。最近 30 天内，每请求 1 只股票的历史 K 线，将会占用 1 个历史 K 线额度。最近 30 天内重复请求同一只股票的历史 K 线，不会重复累计。
举例：
假设您的历史 K 线额度是 100，今天是 2020 年 7 月 5 日。 您在 2020 年 6 月 5 日~2020 年 7 月 5 日之间，共计请求了 60 只股票的历史 K 线，则剩余的历史 K 线额度为 40。

简单来说如果资产不到50w，只能搞300只股票。其实如果只研究头部的话也够用

"""

from mypy import time_maker,sleep
from futu import OpenQuoteContext,KLType,AuType,RET_OK

class futu_new():
    def __init__(self,port=11111,host='127.0.0.1'):
        self.ctx = OpenQuoteContext(host=host, port=port)
    def quota(self, get_detail=True):
        return self.ctx.get_history_kl_quota(get_detail=get_detail)
    # https://openapi.futunn.com/futu-api-doc/quote/get-option-chain.html
    def options(self,ft_code):
        quote_ctx = self.ctx
        ret1, data1 = quote_ctx.get_option_expiration_date(code=ft_code)
        rt = None
        err = None
        if ret1 == RET_OK:
            expiration_date_list = data1['strike_time'].values.tolist()
            for date in expiration_date_list:
                ret2, data2 = quote_ctx.get_option_chain(code=ft_code, start=date, end=date)
                if ret2 == RET_OK:
                    if rt is None: rt = data2
                    else: rt = rt.append( data2 )
                    #print(data2)
                    #print(data2['code'][0])  # 取第一条的股票代码
                    #print(data2['code'].values.tolist())  # 转为 list
                #else: print('error2:', ret2, data2)
                print(data2)
                sleep(1)
        #else: print('error1:',ret1, data1)
        return rt, err

    def pull(self, ft_code, start_date, end_date, ktype=KLType.K_1M, autype=AuType.NONE,
        page_size=1000):
        quote_ctx = self.ctx

        page_num = 0
        page_req_key = None
        rt = None
        err = None
        ret = RET_OK
        while ret == RET_OK:
            page_num += page_size
            print('PULL',ft_code,page_num)
            ret, data, page_req_key = quote_ctx.request_history_kline(ft_code,
                ktype=ktype, autype=autype,
                start=start_date, end=end_date, max_count=page_size,
                page_req_key=page_req_key)
            if ret == RET_OK:
                if rt is None: rt = data
                else: rt = rt.append( data )
                #rt += data['close'].values.tolist()
                #rt += data.values.tolist()
                #print(data)
                #print(data['code'][0])    # 取第一条的股票代码
                #print(data['close'].values.tolist())   # 第一页收盘价转为 list
            else:
                #print('error:', data)
                err = data
                break;
            if page_req_key is None: break
        return rt, err

import os
def exit(rt=0):
    return os._exit(rt)
quit=exit


if __name__ == '__main__':
    # my recent usage...
    # print(quote_ctx.get_history_kl_quota(get_detail=True))
    print(futu_new(host='10.147.19.80').quota())
    quit()

#quote_ctx.close() # 结束后记得关闭当条连接，防止连接条数用尽
