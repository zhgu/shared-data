"""
策略实现基本骨架
作者：小米（债神群）基于 wanjo民科 大佬的 clt_ipc.py

目的：
1、体现一个策略的完整结构，对初学者格外重要，我是这么过来的，所以认为这个工作很有意义（尽量）
2、我多写一点，你就能少写一点，避免重复劳动
3、我趟过的雷，你就没有必要再趟一遍了

定制开发：
若有大佬，觉得这个路数可行，需要策略定制开发，可以联系我，微信号 boymgl
费用 1000 起步，随复杂程度和紧急程度不同酌情增加，非诚勿扰。
0 bug 不能保证，一切尽力而为，充分测试，若真因为 bug 导致了亏损，只能由您自行承担，因为策略无效导致的亏损，也是由您自行承担。
"""

import datetime
import numpy as np
import time

import pandas as pd
from multiprocessing.connection import Client
import sys

sys.path.append('D:\\PycharmProjects\\stock-cli\\shared-data')


def tryx(l, e=print):
    try:
        return l()
    except Exception as ex:
        return ex if True == e else e(ex) if e else None


address = ('127.0.0.1', 16666)


def send_once(v):
    global address
    conn = Client(address)
    conn.send(v)
    rt = conn.recv()
    conn.close()
    return rt


def trd_run():
    import winsound
    from tools.mytdx import mytdx

    # 此处 ip 请到您开户券商提供的通达信PC交易软件的行情站点中取，有时候会变，不是固定的
    tdx = mytdx('124.232.142.23', 7709)

    # 提示音标记变量
    beeped = False

    # 行情循环，每3s读取一次行情
    while True:

        time_str = time.strftime('%H%M%S')
        if time_str < '093000':
            print(f'未开盘')
            time.sleep(3)
            continue
        elif '113000' < time_str < '130000':
            print(f'中午休市')
            time.sleep(3)
            beeped = False
            continue
        elif time_str > '150000':
            print(f'收盘，退出！')

            # 此处可增加盘后处理

            winsound.Beep(4400, 500)
            exit(0)

        # 读取行情，参数是一个字符串数组，存的是6位股票代码，数组长度有限制，不能超过80个
        # 返回的是行情数组，item的结构如下：
        # code: 六位股票代码，字符串
        # market: 市场代码，数字，1 = 上海， 0 = 深圳
        rt = tdx.get_market_records(['601901'])

        if beeped is False:
            # 启动后，首次进入行情循环，给一个提示音
            winsound.Beep(4400, 500)
            beeped = True

        for stock in rt:
            # 处理行情回调，策略主体，一般用于信号计算和下单等核心功能实现，比如
            # 以对手价，买入某标的 100 股
            #   send_once(f"my_order('{stock['code']}',100)")
            # 以限价 50 元，买入某标的 100 股
            #   send_once(f"my_order('{stock['code']}',100, 50)")
            # 是否买入成功，无法得到反馈，这也是不足的地方，决定了该解决方案具备一定的局限性
            # 但是对于非高频策略，流动性充足的情况下，已经够用了，当然可能有比较曲线的解决方案，需要额外实现
            pass

        print(f'行情监听中...')
        time.sleep(3)


if __name__ == '__main__':
    trd_run()
